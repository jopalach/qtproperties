#pragma once

#include <Property.h>
#include <initializer_list>

#include <Enumeration.h>
#include <QHash>

template <typename T>
class EnumerationDisplayProperty
{
public:
    EnumerationDisplayProperty(std::initializer_list<Enumeration<T>> enumerations)
        :
          enumerationProperty_(T())
        , stringProperty_(QString())
    {

        for(auto enumeration : enumerations)
        {
            lookup_.insert(enumeration.value(), enumeration.text());
        }

        QObject::connect(&enumerationProperty_, &I_Property::changed,
            [&](){stringProperty_ = lookup_.value(enumerationProperty_, QString());});
    }
    virtual ~EnumerationDisplayProperty(){}

    EnumerationDisplayProperty& operator=(const T& rhs)
    {
        enumerationProperty_ = rhs;
        return *this;
    }

    Property<QString>& property()
    {
        return stringProperty_;
    }

private:
    Property<T> enumerationProperty_;
    Property<QString> stringProperty_;
    QHash<T, QString> lookup_;

};
