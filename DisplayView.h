#include <QScopedPointer>

#pragma once

#include <QLabel>
#include <Property.h>
#include <EnumerationDisplayProperty.h>

class DisplayView
{
public:
    DisplayView(
        Property<QString>& property,
        QLabel& label)
    {
        QObject::connect(&property, &I_Property::changed,
            [&property, &label](){label.setText(property);});
    }

    virtual ~DisplayView(){}
};

