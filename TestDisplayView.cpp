#include "DisplayView.h"

#include <Property.h>
#include <QtTest/QtTest>
#include <QLabel>
#include <functional>
#include <EnumerationDisplayProperty.h>
#include <Enumeration.h>

enum class Landscapes
{
    Flat,
    Mountains,
    Forest,
    Swamp
};

uint qHash ( const Landscapes& key )
{
    return static_cast<int>(key);
}

Q_DECLARE_METATYPE(Landscapes);

class TestDisplayView : public QObject
{
    Q_OBJECT

public:
   TestDisplayView()
      :selectedLandscapes_({
            {Landscapes::Flat, "flat"},
            {Landscapes::Mountains, "Mountains"},
            {Landscapes::Forest, "Forests"},
            {Landscapes::Swamp, "Swampy"}})
      , view_(selectedLandscapes_.property(), label_)
   {
   }


private slots:

void init()
{
    selectedLandscapes_ = Landscapes::Flat;
}

void updateWidgetWhenPropertyChanges()
{
    QFETCH(Landscapes, value);
    QFETCH(QString, text);
    selectedLandscapes_ = value;

    QCOMPARE(label_.text(), text);
}

void updateWidgetWhenPropertyChanges_data()
{
    QTest::addColumn<Landscapes>("value");
    QTest::addColumn<QString>("text");

    QTest::newRow("flat") << Landscapes::Flat << "flat";
    QTest::newRow("Mountains") << Landscapes::Mountains << "Mountains";
    QTest::newRow("Forests") << Landscapes::Forest << "Forests";
    QTest::newRow("Swampy") << Landscapes::Swamp << "Swampy";
}



private:
    EnumerationDisplayProperty<Landscapes> selectedLandscapes_;
    QLabel label_;
    DisplayView view_;
};



QTEST_MAIN(TestDisplayView)
#include <TestDisplayView.moc>
