#pragma once

#include <QString>
#include <Property.h>

template <typename T>
class Enumeration
{
public:
    Enumeration(T value, QString string)
        :
          value_(value)
        , string_(string)
    {

    }

    virtual ~Enumeration()
    {

    }
    const QString& text() const
    {
        return string_;
    }
    const T& value() const
    {
        return value_;
    }

    operator T&()
    {
        return value_;
    }


private:

    T value_;
    QString string_;
};


